module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'react-hooks'],
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'prettier',
  ],
  rules: {
    // We can turn these 2 off with React 17
    'react/jsx-uses-react': 'off',
    'react/react-in-jsx-scope': 'off',

    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    // This rule incorrectly flags certain inline functions that return JSX, so disable it
    'react/display-name': 'off',
    // No need for React prop types with TypeScript
    'react/prop-types': 'off',

    // Some of the rules regarding `any` type seem too heavy handed so lowering
    // their severity for now
    '@typescript-eslint/no-inferrable-types': 'warn',
    '@typescript-eslint/no-unsafe-call': 'warn',
    '@typescript-eslint/no-unsafe-member-access': 'off',
    '@typescript-eslint/no-unsafe-return': 'off',
    '@typescript-eslint/restrict-template-expressions': 'warn',

    // This rule is particularly difficult to satisfy in a lot of places in our
    // code base without making the code harder to read so turning it off for now
    '@typescript-eslint/no-unsafe-assignment': 'off',

    '@typescript-eslint/ban-ts-comment': [
      'warn',
      // These should be allowed provided they have a proper description
      {
        'ts-ignore': 'allow-with-description',
        'ts-nocheck': 'allow-with-description',
      },
    ],

    // These rules don't seem to warrant 'error' level, so lowering to 'warn'
    '@typescript-eslint/no-floating-promises': 'warn',
    '@typescript-eslint/ban-types': [
      'warn',
      {
        types: {
          // Extending 'object' in a generic param should be acceptable, so turn
          // off this case
          object: false,
        },
      },
    ],
    '@typescript-eslint/no-empty-function': 'warn',

    // Empty interfaces are useful for consistently having named prop types for
    // React components
    '@typescript-eslint/no-empty-interface': 'off',
  },
};
