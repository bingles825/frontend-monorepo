/**
 * Global types for image imports.
 */

/** react-svg-loader allows us to import SVG's as JSX */
declare module '*.svg' {
  const content: (props: { className?: string }) => JSX.Element;
  export default content;
}
declare module '*.jpg';
declare module '*.png';
declare module '*.jpeg';
declare module '*.gif';
