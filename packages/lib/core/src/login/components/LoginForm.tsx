import React from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { Controller, useForm } from 'react-hook-form';
import { ButtonDefault, ButtonNormal } from '../../core';
import { InputField } from '../../forms';
import { colors } from '../../styles';
import { WSHLogo } from '../../assets/svg';

const useLoginStyles = makeStyles((theme: Theme) => ({
  root: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
  },
  logo: {
    marginBottom: 20,
    width: 236,
    '& g': {
      '&:last-child': {
        fill: colors.branding.wsh_slate_grey,
      },
      '&:nth-last-child(2)': {
        fill: colors.branding.wsh_teal,
      },
    },
  },
  errorMessage: {
    fontFamily: theme.typography.body1.fontFamily,
    fontSize: theme.typography.body1.fontSize,
    color: theme.palette.error.main,
    marginBottom: 24,
  },
  form: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
  },
  field: {
    marginBottom: 24,
    width: 218,
    textAlign: 'center',
    '& .MuiInputLabel-root': {
      color: colors.branding.wsh_blue,
      textAlign: 'center',
    },
    '& .MuiInputBase-input': {
      textAlign: 'center',
    },
  },
  button: {
    minWidth: 110,
    '&:first-of-type': {
      marginBottom: 10,
    },
  },
}));

export interface LoginFormData {
  username: string;
  password: string;
}

export interface LoginFormProps {
  onSubmit: (loginData: LoginFormData) => Promise<true | { message: string }>;
}

const LoginForm: React.FC<LoginFormProps> = ({ onSubmit }) => {
  const loginClasses = useLoginStyles();
  const [errorMessage, setErrorMessage] = React.useState('');

  const { control, formState, handleSubmit } = useForm<LoginFormData>({
    mode: 'onSubmit',
    reValidateMode: 'onChange',
  });

  const submit = React.useCallback(
    (loginData: LoginFormData) => {
      onSubmit(loginData).then((resultOrError) => {
        // Set or clear error message
        if (typeof resultOrError === 'object') {
          setErrorMessage(resultOrError.message);
        } else {
          setErrorMessage('');
        }
      });
    },
    [onSubmit],
  );

  return (
    <div className={loginClasses.root}>
      <WSHLogo className={loginClasses.logo} />
      <span className={loginClasses.errorMessage}>{errorMessage}</span>
      <form className={loginClasses.form} onSubmit={handleSubmit(submit)}>
        <Controller
          name="username"
          control={control}
          defaultValue=""
          rules={{ required: 'required' }}
          render={({ field }) => (
            <InputField
              {...field}
              className={loginClasses.field}
              errorText={formState.errors.username?.message}
              label="Login"
            />
          )}
        />
        <Controller
          name="password"
          control={control}
          defaultValue=""
          rules={{ required: 'required' }}
          render={({ field }) => (
            <InputField
              {...field}
              className={loginClasses.field}
              errorText={formState.errors.password?.message}
              label="Password"
              type="password"
            />
          )}
        />
        <ButtonDefault
          className={loginClasses.button}
          disabled={!formState.isValid}
          type="submit"
          enableMinWidth={true}
          text="Login"
        />
        <ButtonNormal
          className={loginClasses.button}
          enableMinWidth={true}
          text="Reset Password"
        />
      </form>
    </div>
  );
};

export default LoginForm;
