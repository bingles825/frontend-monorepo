import InputBaseMUI, { InputBaseProps } from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';
import { inputBase } from '../../styles';

export type { InputBaseProps };

const InputBase = withStyles((theme) => ({
  root: inputBase(theme),
  input: {},
}))(InputBaseMUI);

export default InputBase;
