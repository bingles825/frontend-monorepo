import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import { Dropdown, DropdownProps, InputLabel } from '..';

export interface DropdownFieldProps extends DropdownProps {
  label: string;
}

const DropdownField: React.FC<DropdownFieldProps> = ({
  label,
  id,
  ...dropdownProps
}) => {
  return (
    <FormControl>
      <InputLabel htmlFor={id}>{label}</InputLabel>
      <Dropdown id={id} {...dropdownProps} />
    </FormControl>
  );
};

export default DropdownField;
