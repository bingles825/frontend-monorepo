import React from 'react';
import Select, { SelectProps } from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { InputBase } from '.';
import { makeStyles } from '@material-ui/core';
import { buttonBase } from '../../styles';

const useSelectStyles = makeStyles(() => ({
  select: {
    minWidth: 100,
    '&:focus': {
      backgroundColor: 'transparent',
    },
  },
}));

const useButtonStyles = makeStyles({
  root: buttonBase,
});

export interface DropdownProps extends SelectProps {
  items: { value: number | string; display: React.ReactNode }[];
}

const Dropdown: React.FC<DropdownProps> = ({ items, ...props }) => {
  const selectClasses = useSelectStyles();
  const buttonClasses = useButtonStyles({});

  return (
    <Select
      {...props}
      classes={selectClasses}
      input={<InputBase classes={buttonClasses} />}
      MenuProps={{
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'left',
        },
        getContentAnchorEl: null,
      }}>
      {items.map(({ value, display }) => (
        <MenuItem key={value} value={value}>
          {display}
        </MenuItem>
      ))}
    </Select>
  );
};

export default Dropdown;
