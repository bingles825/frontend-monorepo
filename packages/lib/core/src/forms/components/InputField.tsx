import React from 'react';
import FormControl, { FormControlProps } from '@material-ui/core/FormControl';
import { InputBase, InputBaseProps, InputLabel } from '.';
import InputAdornment from '@material-ui/core/InputAdornment';
import ErrorIcon from '@material-ui/icons/Error';

export interface InputFieldProps extends FormControlProps {
  label: string;
  type?: InputBaseProps['type'];
  value?: string;
  errorText?: string;
}

const InputField: React.FC<InputFieldProps> = React.forwardRef(
  (
    { label, errorText, type, placeholder, value, defaultValue, ...props },
    ref,
  ) => {
    const inputId = props.id && `${props.id}-input`;

    return (
      <FormControl ref={ref} {...props} error={errorText != null}>
        <InputLabel htmlFor={inputId}>{label}</InputLabel>
        <InputBase
          id={inputId}
          placeholder={placeholder}
          type={type}
          defaultValue={defaultValue}
          value={value}
          endAdornment={
            errorText != null ? (
              <InputAdornment position="end">
                <ErrorIcon color="error" />
              </InputAdornment>
            ) : null
          }
        />
        {/* {errorText != null && (
        <FormHelperText error={errorText != null} id={`${id}-helper-text`}>
          {errorText}
        </FormHelperText>
      )} */}
      </FormControl>
    );
  },
);

export default InputField;
