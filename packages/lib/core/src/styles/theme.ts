import { createTheme } from '@material-ui/core';
import colors from './colors';

const fontFamily = [
  '"Avenir"',
  '"Arial"',
  '"Segoe UI"',
  'helvetica',
  'verdana',
  'sans-serif',
].join(',');

export const theme = createTheme({
  typography: {
    fontFamily,
    fontSize: 13,
    button: {
      fontSize: 13,
      textTransform: 'none',
    },
    h1: {
      color: colors.branding.wsh_blue,
      fontFamily: `Avenir Black,${fontFamily}`,
      fontSize: 24,
      fontWeight: 'bold',
    },
    h2: {
      color: colors.branding.wsh_blue,
      fontFamily: `Avenir Black,${fontFamily}`,
      fontSize: 18,
      fontWeight: 'bold',
    },
    h3: {
      color: colors.branding.wsh_blue,
      fontFamily: `Avenir Heavy,${fontFamily}`,
      fontSize: 13,
      fontWeight: 'bold',
    },
    body1: {
      fontFamily: `Avenir Medium,${fontFamily}`,
      fontSize: 13,
      fontWeight: 'normal',
    },
  },
  palette: {
    error: {
      main: colors.theme.danger,
    },
  },
});
console.log('theme:', theme);
