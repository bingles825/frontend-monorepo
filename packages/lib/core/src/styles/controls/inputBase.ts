import { Theme } from '@material-ui/core';
import { CSSProperties } from '@material-ui/core/styles/withStyles';
import { controlBase, ControlVariant } from '..';
import colors from '../colors';

export const inputBase =
  (theme: Theme) =>
  (props: { controlVariant?: ControlVariant }): CSSProperties => {
    const buttonColors = colors.controls[props.controlVariant ?? 'normal'];

    return {
      ...controlBase(props),
      backgroundColor: colors.controls.input_background,
      borderColor: colors.theme.border,
      color: buttonColors.up.text,
      '&:hover,&:focus': {
        borderColor: colors.controls.default.hover.border,
      },
      '&:active': {
        borderColor: colors.controls.default.selected.border,
      },
      '&.Mui-error': {
        borderColor: theme.palette.error.main,
      },
      '&.MuiInputAdornment-root': {
        color: theme.palette.error.main,
      },
    };
  };
