import { CSSProperties } from '@material-ui/core/styles/withStyles';

export type ControlVariant = 'normal' | 'default';

export const controlBase = ({
  controlVariant = 'normal',
}: {
  controlVariant?: ControlVariant;
}): CSSProperties => {
  return {
    alignItems: 'center',
    border: '1px solid',
    borderRadius: 4,
    borderWidth: 1,
    boxShadow: 'none',
    boxSizing: 'border-box',
    display: 'inline-flex',
    fontSize: 13,
    fontWeight: 'normal',
    height: 32,
    padding: '0 8px',
    width: 'auto',
  };
};
