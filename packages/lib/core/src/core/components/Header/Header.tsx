import React from 'react';
import Typography from '@material-ui/core/Typography';

export interface HeaderProps {
  text?: string;
}

const withVariant = (variant: 'h1' | 'h2' | 'h3'): React.FC<HeaderProps> => {
  return ({ children, text }) => {
    return <Typography variant={variant}>{text ?? children}</Typography>;
  };
};

export const Header1 = withVariant('h1');
export const Header2 = withVariant('h2');
export const Header3 = withVariant('h3');
