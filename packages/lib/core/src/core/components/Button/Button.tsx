import ButtonMUI, {
  ButtonProps as ButtonMUIProps,
} from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { buttonBase, ControlVariant } from '../../../styles';

export interface ButtonProps extends ButtonMUIProps {
  controlVariant?: ControlVariant;
  enableMinWidth?: true;
  text?: string;
}

const useButtonStyles = makeStyles({
  root: (
    props: Pick<ButtonProps, 'controlVariant' | 'enableMinWidth' | 'text'>,
  ) => ({
    ...buttonBase(props),
    minWidth: props.enableMinWidth ? 96 : undefined,
  }),
});

/** Higher-order component to set the controlVariant */
const withButtonVariant =
  (controlVariant: ControlVariant): React.FC<ButtonProps> =>
  ({ children, enableMinWidth, text, ...props }) => {
    const classes = useButtonStyles({ controlVariant, enableMinWidth });

    return (
      <ButtonMUI classes={classes} disableRipple={true} {...props}>
        {text ?? children}
      </ButtonMUI>
    );
  };

export const ButtonNormal = withButtonVariant('normal');
export const ButtonDefault = withButtonVariant('default');
