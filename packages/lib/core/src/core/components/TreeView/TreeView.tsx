import React from 'react';
import MUITreeView from '@material-ui/lab/TreeView';
import MUITreeItem from '@material-ui/lab/TreeItem';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

export interface TreeNode {
  id: string;
  parentId: string;
  label: string;
}

export interface TreeViewProps {
  nodes: TreeNode[];
}

const DeploymentTree: React.FC<TreeViewProps> = ({ nodes }) => {
  const { ids, groups } = React.useMemo(() => {
    const ids = {};

    const groups = nodes.reduce((grouped, node) => {
      ids[node.id] = node.parentId;
      grouped[node.parentId] = grouped[node.parentId] ?? {};
      grouped[node.parentId][node.id] = node;
      return grouped;
    }, {});

    return {
      ids,
      groups,
    };
  }, [nodes]);

  console.log(ids, groups);

  function renderTree(id: string) {
    const groupId = ids[id];
    const node = groups[groupId][id];
    const childIds = Object.keys(groups[id] ?? {});
    return (
      <MUITreeItem key={id} nodeId={id} label={node.label}>
        {childIds.map(renderTree)}
      </MUITreeItem>
    );
  }

  const rootIds = Object.keys(groups['ROOT_NODE_PARENT_ID']);

  return (
    <div>
      <MUITreeView
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpanded={rootIds}
        defaultExpandIcon={<ChevronRightIcon />}>
        {rootIds.map(renderTree)}
      </MUITreeView>
    </div>
  );
};

export default DeploymentTree;
