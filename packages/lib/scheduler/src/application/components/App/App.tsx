import React from 'react';
import { ButtonDefault } from '@wsh/core';

export interface AppProps {}

const App: React.FC<AppProps> = ({}) => {
  return (
    <div>
      App
      <ButtonDefault />
    </div>
  );
};

export default App;
