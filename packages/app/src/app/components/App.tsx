import React from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import Admin from './Admin';
import Denials from './Denials';

export interface AppProps {}

const App: React.FC<AppProps> = () => {
  return (
    <Router>
      <div>Header</div>
      <Link to="/">Home</Link>
      &nbsp;
      <Link to="/admin">Admin</Link>
      &nbsp;
      <Link to="/denials">Denials</Link>
      <div>
        <Route path="/">Home</Route>
        <Route path="/admin" component={Admin} />
        <Route path="/denials" component={Denials} />
      </div>
    </Router>
  );
};

export default App;
