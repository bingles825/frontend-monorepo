import React from 'react';

const ButtonDefault = React.lazy(() =>
  import('@wsh/core').then((module) => ({ default: module.ButtonDefault })),
);

export interface AdminProps {}

const Admin: React.FC<AdminProps> = () => {
  return (
    <React.Suspense fallback={<div>Loading...</div>}>
      <ButtonDefault>Test</ButtonDefault>
    </React.Suspense>
  );
};

export default Admin;
