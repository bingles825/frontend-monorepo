import React from 'react';
import { Story, Meta } from '@storybook/react';
import { TreeView, TreeViewProps } from '@wsh/core';
export default {
  title: 'Components/TreeView',
  component: TreeView,
} as Meta;

const Template: Story<TreeViewProps> = (args) => {
  const [nodes, setNodes] = React.useState<TreeNode[]>(() => [
    {
      id: '1',
      parentId: 'ROOT_NODE_PARENT_ID',
      label: 'One',
    },
    {
      id: '2',
      parentId: '1',
      label: 'Two',
    },
    {
      id: '3',
      parentId: '1',
      label: 'Three',
    },
    {
      id: '4',
      parentId: '2',
      label: 'Four',
    },
    {
      id: '5',
      parentId: '2',
      label: 'Five',
    },
  ]);
  return (
    <div>
      <button
        onClick={() =>
          setNodes((nodes) => [
            ...nodes,
            { id: '6', parentId: '5', label: 'Six' },
          ])
        }>
        Add
      </button>
      <TreeView {...args} nodes={nodes} />
    </div>
  );
};

export const TreeViewDefault = Template.bind({});
