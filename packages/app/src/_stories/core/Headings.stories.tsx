import { Story, Meta } from '@storybook/react';
import Typography from '@material-ui/core/Typography';

export default {
  title: 'Core/Headings',
  component: Typography,
} as Meta;

export const Headings: Story = () => (
  <div>
    <Typography variant="h1">Header 1</Typography>
    <Typography variant="h2">Header 2</Typography>
    <Typography variant="h3">Header 3</Typography>
    <Typography variant="body1">Body</Typography>
  </div>
);
