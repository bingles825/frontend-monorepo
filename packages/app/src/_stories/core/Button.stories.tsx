import { Story, Meta } from '@storybook/react';
import { ButtonDefault, ButtonNormal, Header2 } from '@wsh/core';

export default {
  title: 'Core/Button',
  component: ButtonNormal,
} as Meta;

export const ButtonNormalStory: Story = () => {
  return (
    <div>
      <Header2 text="Normal Button" />
      <ButtonNormal>Enabled</ButtonNormal>&nbsp;
      <ButtonNormal disabled={true}>Disabled</ButtonNormal>
    </div>
  );
};
ButtonNormalStory.storyName = 'Normal Button';

export const ButtonDefaultStory: Story = () => {
  return (
    <div>
      <Header2 text="Default Button" />
      <ButtonDefault>Enabled</ButtonDefault>&nbsp;
      <ButtonDefault disabled={true}>Disabled</ButtonDefault>
    </div>
  );
};
ButtonDefaultStory.storyName = 'Default Button';
