import React from 'react';
import { Story, Meta } from '@storybook/react';
import {
  ButtonDefault,
  ButtonNormal,
  DropdownField,
  Header1,
  Header2,
  Header3,
  InputField,
} from '@wsh/core';

export interface StyleguideProps {}

const Styleguide: React.FC<StyleguideProps> = ({}) => {
  const [number, setNumber] = React.useState<'--' | number>('--');

  return (
    <div>
      <h2>Headers</h2>
      <Header1 text="Header 1" />
      <Header2 text="Header 2" />
      <Header3 text="Header 3" />
      <h2>Buttons</h2>
      <Header3>Normal Button</Header3>
      <ButtonNormal>Enabled</ButtonNormal>&nbsp;
      <ButtonNormal disabled={true}>Disabled</ButtonNormal>
      <Header3>Default Button</Header3>
      <ButtonDefault>Enabled</ButtonDefault>&nbsp;
      <ButtonDefault disabled={true}>Disabled</ButtonDefault>
      <Header3>Min Width Button</Header3>
      <ButtonNormal enableMinWidth={true}>96px</ButtonNormal>&nbsp;
      <ButtonDefault enableMinWidth={true}>96px</ButtonDefault>
      <h2>Input Field</h2>
      <InputField label="First Name" />
      <br />
      <DropdownField
        label="Tens"
        value={number}
        onChange={(event) => setNumber(event.target.value as '--' | number)}
        items={[
          { value: '--', display: <em>--</em> },
          { value: 10, display: 'Ten' },
          { value: 20, display: 'Twenty' },
          { value: 30, display: 'Thirty' },
        ]}
      />
    </div>
  );
};

export default {
  title: 'Styleguide',
  component: Styleguide,
} as Meta;

const Template: Story<StyleguideProps> = (args) => <Styleguide {...args} />;

export const StyleguideDefault = Template.bind({});
StyleguideDefault.storyName = 'Styleguide';
