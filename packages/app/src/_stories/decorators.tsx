import { ThemeProvider } from '@material-ui/core';
import { theme } from '@wsh/core';

export const themeDecorator = (Story: () => JSX.Element) => (
  <ThemeProvider theme={theme}>
    <Story />
  </ThemeProvider>
);
