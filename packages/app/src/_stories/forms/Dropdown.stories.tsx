import { Story, Meta } from '@storybook/react';
import { Dropdown, DropdownProps } from '@wsh/core';

export default {
  title: 'Forms/Dropdown',
  component: Dropdown,
} as Meta;

const Template: Story<DropdownProps> = (args) => <Dropdown {...args} />;

export const DropdownDefault = Template.bind({});
DropdownDefault.args = {
  value: 10,
  items: [
    { value: '', display: <em>None</em> },
    { value: 10, display: 'Ten' },
    { value: 20, display: 'Twenty' },
    { value: 30, display: 'Thirty' },
  ],
};
