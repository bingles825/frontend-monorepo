import { Story, Meta } from '@storybook/react';
import { LoginForm, LoginFormProps } from '@wsh/core';

export default {
  title: 'Components/LoginForm',
  component: LoginForm,
} as Meta;

const Template: Story<LoginFormProps> = (args) => <LoginForm {...args} />;

export const LoginFormDefault = Template.bind({});
LoginFormDefault.args = {
  onSubmit: async ({
    username,
    password,
  }): Promise<true | { message: string }> => {
    if (username === 'johndoe' && password === 'pwd') {
      return true;
    }

    return { message: 'Invalid Login or Password' };
  },
};
