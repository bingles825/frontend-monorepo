import { themeDecorator } from '../src/_stories/decorators';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    // Disable controls by default
    disable: true,
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  // Hide actions panel by default
  options: { showPanel: false },
};

export const decorators = [themeDecorator];
