const path = require('path');

module.exports = {
  core: {
    builder: 'webpack5',
  },
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials'],
  webpackFinal: async (config) => {
    // SVG's are already handled by Storybook's default webpack config, so we
    // have to exclude them from the default rule
    config.module.rules = config.module.rules.map((rule) => {
      let testStr = rule.test.toString();

      if (testStr.includes('svg|')) {
        // The default image loader has a test RegExp that looks like
        // /\.(svg|ico|jpg|jpeg|png|apng|gif|eot|otf|webp|ttf|woff|woff2|cur|ani|pdf)(\?.*)?$/
        // so we strip out any image types we want to handle on our own
        testStr = testStr.replace('svg|', '');

        // strip the leading and trailing '/' from the original regex string
        testStr = testStr.replace(/\//g, '');

        return {
          ...rule,
          test: new RegExp(testStr),
        };
      }

      return rule;
    });

    config.module.rules.push(
      // Sass rule
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          // resolve-url-loader is needed to resolve relative url paths inside
          // of scss files.
          'resolve-url-loader',
          'sass-loader',
        ],
        include: path.resolve(__dirname, '../'),
      },
      // Add our SVG loader rule
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              // This allows us to configure Babel in a babel.config.js in our
              // monorepo root
              rootMode: 'upward',
            },
          },
          {
            loader: 'react-svg-loader',
            options: {
              jsx: true, // true outputs JSX tags
            },
          },
        ],
      },
    );

    return config;
  },
};
