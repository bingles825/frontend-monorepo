const HtmlWebpackPlugin = require('html-webpack-plugin');
// const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const path = require('path');

module.exports = (env) => ({
  entry: './src/bootstrap',
  mode: 'production',
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, 'dist'),
    // publicPath: '/',
    // clean: true
  },
  resolve: {
    extensions: ['.Webpack.js', '.web.js', '.ts', '.js', '.jsx', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'ts-loader',
        options: {
          compilerOptions: {
            noEmit: false,
          },
          configFile: 'tsconfig.build.json',
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Resolve relative url paths inside of scss files.
          'resolve-url-loader',
          // Compiles Sass to CSS
          {
            loader: 'sass-loader',
            options: {
              // resolve-url-loader requires that any loader that runs before it
              // include source maps
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.css$/,
        //hot reload css
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader',
          },
          {
            loader: 'react-svg-loader',
            options: {
              jsx: true, // true outputs JSX tags
            },
          },
        ],
      },
      {
        test: /\.(png|jpg|gif|ico)$/,
        exclude: /(node_modules|bower_components)/,
        use: ['file-loader?name=static/images/[name].[ext]'],
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        use: 'url-loader?name=[name].[ext]',
      },
    ],
  },
  plugins: [
    // new ModuleFederationPlugin({
    //   name: 'platformApp',
    //   filename: 'remoteEntry.js',
    //   remotes: {
    //     denialApp: `denialApp@https://denial.test.wshcloud.com/remoteEntry.js`,
    //     schedulerApp: `schedulerApp@https://scheduler.test.wshcloud.com/remoteEntry.js`,
    //   },
    //   shared: {
    //     'css-loader': {},
    //     'devextreme-react': {},
    //     'style-loader': {},
    //     'sass-loader': {},
    //     'prop-types': {},
    //     react: {
    //       singleton: true,
    //       requiredVersion: '17.0.2',
    //     },
    //     'react-dom': {
    //       singleton: true,
    //       requiredVersion: '17.0.2',
    //     },
    //     'react-router': {
    //       singleton: true,
    //     },
    //     'react-router-dom': {
    //       singleton: true,
    //     },
    //   },
    // }),
    new HtmlWebpackPlugin({
      //title: 'Whitespace',
      template: 'index.html',
    }),
  ],
});
