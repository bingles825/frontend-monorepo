const HtmlWebpackPlugin = require('html-webpack-plugin');
// const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const path = require('path');

module.exports = {
  devtool: 'inline-source-map',
  devServer: {
    //  hot: true,
    contentBase: path.join(__dirname, 'dist'),
    //  historyApiFallback: true,
    proxy: {
      '/api': {
        target: 'https://localhost:44385',
        secure: false,
      },
    },
  },
  entry: './src/index',
  // entry: {
  //   app: './src/bootstrap'
  // },
  mode: 'development',
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: 'auto',
    // publicPath: 'auto',
    clean: true,
  },
  resolve: {
    extensions: ['.Webpack.js', '.web.js', '.ts', '.js', '.jsx', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'ts-loader',
        options: {
          compilerOptions: {
            noEmit: false,
          },
          configFile: 'tsconfig.build.json',
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Resolve relative url paths inside of scss files.
          'resolve-url-loader',
          // Compiles Sass to CSS
          {
            loader: 'sass-loader',
            options: {
              // resolve-url-loader requires that any loader that runs before it
              // include source maps
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.css$/,
        //hot reload css
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader',
          },
          {
            loader: 'react-svg-loader',
            options: {
              jsx: true, // true outputs JSX tags
            },
          },
        ],
      },
      {
        test: /\.(png|jpg|gif|ico)$/,
        exclude: /(node_modules|bower_components)/,
        use: ['file-loader?name=static/images/[name].[ext]'],
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        use: 'url-loader?name=[name].[ext]',
      },
    ],
  },
  plugins: [
    // new ModuleFederationPlugin({
    //   name: 'platformApp',
    //   filename: 'remoteEntry.js',
    //   remotes: {
    //     denialApp: 'denialApp@http://localhost:8082/remoteEntry.js',
    //     schedulerApp: 'schedulerApp@http://localhost:8081/remoteEntry.js',
    //   },
    //   shared: {
    //     'css-loader': {},
    //     'devextreme-react': {},
    //     'style-loader': {},
    //     'sass-loader': {},
    //     'prop-types': {},
    //     react: {
    //       requiredVersion: '17.0.2',
    //       singleton: true,
    //     },
    //     'react-dom': {
    //       requiredVersion: '17.0.2',
    //       singleton: true,
    //     },
    //     'react-router': {
    //       singleton: true,
    //     },
    //     'react-router-dom': {
    //       singleton: true,
    //     },
    //   },
    // }),
    new HtmlWebpackPlugin({
      title: 'Hot Module Replacement',
      template: 'index.html',
    }),
  ],
};
