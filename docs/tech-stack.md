# WhiteSpace Frontend Tech Stack

- TypeScript
- React - frontend comonent framework
- Redux Toolkit - global state management
- React Router - routing
- Webpack - code bundling
- Prettier - code formatting

# Proposed Changes

- ~~Devextreme~~ - we have been fighting the API since the underlying framework was not designed for React
- ~~Module Federation~~ - We are not getting any benefit using module federation and are even losing TypeScript type safety across package boundaries.
- Sass - if we go with Material UI, there is a built in css in js framework that will replace most of our Sass usage for components. TBD: whether we will still need Sass for some things
- Azure Pipelines - CI/CD should run build + tests. Recommending we have a dedicated git repo + pipeline for frontend codebase.
- Eslint - code validation + formatting (combined with Prettier)
- Husky + lint-staged - git precommit hooks to run Prettier + eslint
- Storybook - component design / showcase
- Jest - unit testing
- yarn workspaces - to replace `npm` so that we can leverage monorepo features
- Material UI - to replace Devextreme (still needs a little more vetting. See the [matieral-ui](https://bitbucket.org/bingles825/frontend-monorepo/src/material-ui/) branch of this repo)
- react-hook-form - React hooks for forms + validation
