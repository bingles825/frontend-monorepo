# Frontend Naming Conventions

## Files and Directories

The default convention for folder and file names is `camelCase`. Exceptions to this rule are React component names and ES6 class names which should be `PascalCase`.

Certain file types may also employ a sub-extension to make them easier to identify. Common ones are `.fetch.ts`, `.model.ts`, `.action.ts`, `.selector.ts`, and `.slice.ts`.

Here's an example file tree using our conventions.

```
src/
  someFeature/
    components/
      ComponentX/
        ComponentX.tsx
        ComponentX.scss
      ComponentY.tsx
    data/
      someFeature.fetch.ts
      someFeature.model.ts
    state/
      someFeature.action.ts
      someFeature.selector.ts
      someFeature.slice.ts
    util/
      someUtil.ts
```

## Variable Names

Variable names and function names should generally be `pascalCase` with the exception being ES6 class names and React component names.

```typescript
const someVar = 4;

function someOther(): void {}
```

```typescript
const SomeComponent: React.FC<SomeComponentProps> = (props) => { ... }

function SomeOtherComponent(props: SomeComponentProps): JSX.Element { ... }

class SomeClass {}
```

Constants that are intended to be global static values (not to be confused with general `const`) can be `UPPER_DASH_CASE` to make them easier to identify.

```typescript
export const MAX_LOGIN_ATTEMPTS = 3;
```

## Interfaces and Types

Interfaces and types should be `PascalCase`. Interfaces should _not_ use the `I` prefix.

```typescript
export interface SomeProps {}

export type Name = 'firstName' | 'lastName';
```
