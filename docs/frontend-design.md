# Frontend Design

## High Level Design

![Design](images/frontend-design.drawio.png)

## App and Lib Packages

![Features and Dependencies](images/features-and-dependencies.drawio.png)
