# Frontend Reference Architecture

This is a monorepo intended to illustrate the architecture and design patterns used in the WhiteSpace Health frontend codebase.

For a high-level architecture overview, see
[Frontend Design](docs/frontend-design.md)

For details on our technology stack, see
[Tech Stack](docs/tech-stack.md)

For details on recommended naming conventions, see [Front End Naming Conventions](docs/naming-conventions.md)

## Monorepo

This is a `yarn` monorepo that contains various `npm` packages representing frontend applications and shared libraries. Each `npm` package lives under the `packages` folder.

- app/ - the frontend application
- lib/
  - admin/ - code for the admin tool
  - core/ - code that is shared across all features (e.g. buttons, logins, inputs, etc.)
  - denials - code specific to denials product
  - scheduler - code specific to the scheduler product

![Monorepo](docs/images/monorepo-packages.png)

## Source Code

Within each lib and app folder, all source code should live under a `src` folder.

The general pattern for structuring code is:

```
src/
  featureAaa/
    components/
      ComponentX.tsx
      ComponentY.tsx
    data/
      aaa.fetch.ts
      aaa.model.ts
    state/
      aaa.action.ts
      aaa.selector.ts
      aaa.slice.ts
    util/
      aaaUtil.ts
  featureBBB/
    components/
    data/
    state/
    util/
  shared/
    components/
    data/
    state/
    util/
```

### Features

Each `npm` package contains 1-n features. Each feature has its own top-level folder directly under `src`.

![Features](docs/images/features.png)

### Feature Sections

Each feature can contain various sections such as models, utilities, business logic, and `React` components. Common subfolders here are `components`, `state`, `data`, and `util`

![Feature Sections](docs/images/feature-contents.png)

### Components

React components should live under `src/someFeature/components` folders.

![Components Folder](docs/images/components-folder.png)

In cases where the number of files under `components` gets too large, additional
subfolders can be created for specific components.

![Nested Components Folder](docs/images/components-folder-nested.png)

### State

Features that use Redux should have a `src/someFeature/state` folder. It should contain any state slice, actions, selectors, or reducers.

![State Folder](docs/images/state-folder.png)

### Data

Features that fetch data or define types for some sort of data model should have a `src/someFeature/data` folder. Data model definitions and business logic should be separated from data fetching code.

![Data Folder](docs/images/data-folder.png)

### Util

Some features require additional utility functions, types, etc. They should live in a `src/someFeature/util` folder.

### Barrel Files (index.ts)

Each directory (and subdirectory) should include an `index.ts` file that re-exports the items contained in its containing folder. This is commonly called a "barrel file" and makes it easier for consumers to import things without requiring long import paths. It also makes it easier to refactor code while minimizing the need to change import paths.

![Barrel Files](docs/images/barrel-files.png)
